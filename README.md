# JamesDSPManager (Audio Effect Digital Signal Proccessing library for Android)
Merged with Omnirom DSP Manager features and able to run in all android rom include Samsung,AOSP,Cyanogenmod. 
This app in order to improve your music experience especially you want realistic bass and more natural clarity.
We don't work too much around with modifying Android framework instead of we integrate framework with DSP Manager, let it less depend on system framework.

JamesDSPManager merge with Omnirom features: 
Basic: 
1.Compression 
2.Bass Boost
3.Equalizer (8 Bands HS Filter)
4.Virtualizer
Omnirom extra:
1.Adjustable Low pass filter(Bass Boost)
2.Stereo Widen

Now work on AOSP, Cyanogenmod, Samsung on Android 5.0 (TESTED)

#Download Link
Lollipop: https://drive.google.com/open?id=0B5xvL_71lUP_c1pIT2pZOEdERVE
Marshmallow: https://drive.google.com/open?id=0B5xvL_71lUP_VXJBdHc2MXFKUk0
Lollipop(百度連結): http://pan.baidu.com/s/1kTZgbGV

#Development
For user want to try eclipse_build_test_libjamesdsp. This is a testing library that doesn't work properly at all, I might use this in future when I solve problems.

#Important
We won't modify SELinux, let your device become more safe.
Also, it is good for you to customize your own rom or even port rom, upgrade your music experience!

#How to install?
See readme in download link.

#Credit
DSPFilter.xlsx is a tool for you to desgin IIR Biquad Filter, it is a component from miniDSP.